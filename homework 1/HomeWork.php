<?php

function isNegative($number) {
    if ($number < 0) {
        echo "Число $number отрицательное\n";
    } else {
        echo "Число $number не отрицательное\n";
    }
}
function stringLength($string) {
    echo "Длина строки: " . strlen($string) . "\n";
}
function containsA($string) {
    if (strpos($string, 'a') !== false) {
        echo "да\n";
    } else {
        echo "нет\n";
    }
}
function checkDivisibility1($number) {
    $divisors = [2, 3, 5, 6, 9];
    foreach ($divisors as $divisor) {
        if ($number % $divisor === 0) {
            echo "Число $number делится на $divisor без остатка\n";
        } else {
            echo "Число $number не делится на $divisor без остатка\n";
        }
    }
}
function checkDivisibility2($number) {
    $divisors = [3, 5, 7, 11];
    foreach ($divisors as $divisor) {
        if ($number % $divisor === 0) {
            echo "Число $number делится на $divisor без остатка\n";
        } else {
            echo "Число $number не делится на $divisor без остатка\n";
        }
    }
}
function lastCharacter($string) {
    echo "Последний символ строки: " . $string[strlen($string) - 1] . "\n";
}
function lastCharacterAgain($string) {
    echo "Последний символ строки: " . $string[strlen($string) - 1] . "\n";
}
function triangleArea($base, $height) {
    $area = 0.5 * $base * $height;
    echo "Площадь треугольника: $area\n";
}
function rectangleArea($length, $width) {
    $area = $length * $width;
    echo "Площадь прямоугольника: $area\n";
}
function squareNumber($number) {
    $square = $number * $number;
    echo "Квадрат числа $number: $square\n";
}
// Задача 1
isNegative(-5);

// Задача 2
stringLength("Пример строки");

// Задача 3
containsA("Пример");

// Задача 4
checkDivisibility1(30);

// Задача 5
checkDivisibility2(35);

// Задача 6
lastCharacter("Пример");

// Задача 7
lastCharacterAgain("Пример");

// Задача 8
triangleArea(10, 5);

// Задача 9
rectangleArea(10, 20);

// Задача 10
squareNumber(4);
